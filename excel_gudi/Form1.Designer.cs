﻿namespace excel_gudi
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewFileName = new System.Windows.Forms.ListView();
            this.openFile = new System.Windows.Forms.Button();
            this.copySheet = new System.Windows.Forms.Button();
            this.labelInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listViewFileName
            // 
            this.listViewFileName.AllowColumnReorder = true;
            this.listViewFileName.AllowDrop = true;
            this.listViewFileName.Location = new System.Drawing.Point(125, 12);
            this.listViewFileName.MinimumSize = new System.Drawing.Size(100, 100);
            this.listViewFileName.Name = "listViewFileName";
            this.listViewFileName.Size = new System.Drawing.Size(258, 100);
            this.listViewFileName.TabIndex = 0;
            this.listViewFileName.UseCompatibleStateImageBehavior = false;
            this.listViewFileName.View = System.Windows.Forms.View.List;
            // 
            // openFile
            // 
            this.openFile.Location = new System.Drawing.Point(22, 12);
            this.openFile.Name = "openFile";
            this.openFile.Size = new System.Drawing.Size(75, 32);
            this.openFile.TabIndex = 1;
            this.openFile.Text = "Open Files";
            this.openFile.UseVisualStyleBackColor = true;
            this.openFile.Click += new System.EventHandler(this.openFile_Click);
            // 
            // copySheet
            // 
            this.copySheet.Location = new System.Drawing.Point(22, 68);
            this.copySheet.Name = "copySheet";
            this.copySheet.Size = new System.Drawing.Size(75, 44);
            this.copySheet.TabIndex = 2;
            this.copySheet.Text = "Process && Save";
            this.copySheet.UseVisualStyleBackColor = true;
            this.copySheet.Click += new System.EventHandler(this.copySheet_Click);
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.Location = new System.Drawing.Point(19, 123);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(33, 13);
            this.labelInfo.TabIndex = 3;
            this.labelInfo.Text = "Label";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 145);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.copySheet);
            this.Controls.Add(this.openFile);
            this.Controls.Add(this.listViewFileName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MinimumSize = new System.Drawing.Size(100, 100);
            this.Name = "Form1";
            this.Text = "Excel Batch Processing by Yueyi Zhang";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewFileName;
        private System.Windows.Forms.Button openFile;
        private System.Windows.Forms.Button copySheet;
        private System.Windows.Forms.Label labelInfo;
    }
}

